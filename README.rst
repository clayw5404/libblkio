========
libblkio
========
------------------------
Block device I/O library
------------------------
libblkio is a library for high-performance block device I/O with support for
multi-queue devices. A C API is provided so that applications can use the
library from most programming languages.

Applications are increasingly integrating high-performance I/O interfaces such
as Linux io_uring, userspace device drivers, and vhost-user device support. The
effort required to add each of these low-level interfaces into an application
is relatively high. libblkio provides a single API for efficiently accessing
block devices and eliminates the need to write custom code for each one.

libblkio is currently under development and not yet stable. It supports Linux
io_uring with the future addition of userspace NVMe, virtio-blk, and vhost-user
drivers planned.

This library is licensed under either the MIT or Apache 2.0 license at your
option.

Documentation
-------------
API documentation is `here <docs/blkio.rst>`_.

Examples
--------
Example programs are located in the `examples/ <examples/>`_ directory and
demonstrate how to use the API.

Installation
------------
The following steps install the library on the local system::

  # dnf install -qy meson rust cargo python3-docutils rustfmt # Fedora/CentOS/RHEL
  # apt-get install -qy meson rustc cargo python3-docutils # Debian/Ubuntu
  # meson setup build
  # meson compile -C build
  # meson install -C build

Development
-----------
Configure a debug build (unoptimized and with debug info) using::

  $ meson setup build --buildtype=debug

Or compile and test locally (also with a debug build) using::

  $ ./containerized-build.sh

This script requires `podman <https://podman.io/>`_ and `buildah
<https://buildah.io/>`_. It should be possible to use Docker with minimal
changes, but the script currently does not support it.

Continuous Integration
----------------------
All merge requests must pass the CI system. `GitLab CI
<https://docs.gitlab.com/ee/ci/>`_ is used for continuous integration. See
`.gitlab-ci.yml` for details of the jobs. If you fork the repository on GitLab
then the CI will run on your personal branches too.

Contact
-------
For questions and discussion, join the `Matrix chat room at
#libblkio:matrix.org <https://matrix.to/#/#libblkio:matrix.org>`_.

Please report bugs and request features on the `issue tracker
<https://gitlab.com/libblkio/libblkio/-/issues>`_.
